
--EXPLAIN
SELECT 
       SUM(TOTALAA)         AS AnioAnt,
       SUM(TOTALSA)         AS SemAnt,
       SUM(OBJETIVO)        AS Objetivo,
       SUM(COLOCACIONBRUTA)  AS 'Real',
       SUM(COLOCACIONBRUTA) - SUM(OBJETIVO) AS VsObjetivo,
       SUM(COLOCACIONBRUTA) - SUM(TOTALSA) AS VsSemAnt, 
       SUM(COLOCACIONBRUTA) - SUM(TOTALAA) AS VsAnioAnt, 
       FIORIGEN             AS Id,
       FcDescOrigen         AS Descripcion
FROM (
        SELECT SUM(DC.FNCOLOCABRUTA) AS TOTALAA,
               0 AS TOTALSA,
               0 AS OBJETIVO,
               0 AS COLOCACIONBRUTA,
               DC.EsSucursal AS FIORIGEN,
               DC.FCSUCURSALORIGEN AS FcDescOrigen -- PADRE
               --DC.FCORIGEN AS FcDescOrigen --HIJO
          FROM COLOCACION_AGRUPADA DC
         WHERE 1 = 1 
           AND DC.FDFECHA BETWEEN '2020-07-26' AND '2020-07-26'
           AND DC.ESTERRITORIO = 1
           AND DC.ESSUCURSAL IS NOT NULL
           --AND DC.FISUPERIOR = 12 --Se habilita si se selecciona un elemento padre para colocarle como parámetro el elemento hijo, sino se deshabilita
           --AND DC.FICLASIFICACIONCTE IN(-1, 0, 1) --se agrega a la consulta solo si se seleccion? alg?n elemento del dropdownlist
           --AND FIELEMENTO_PADRE = 10000007 --Condici?n que se enlaza en caso de traer el par?metro para formato
           --AND DC.FIDIASEM BETWEEN 1 AND 7 -- par?metro del tabular d?a
      GROUP BY DC.EsSucursal,
               DC.FCSUCURSALORIGEN --Se habilita si lo que se va a mostrar es el elemento padre
               --DC.FCORIGEN --Se habilita si lo que se va a mostrar es el elemento hijo
  UNION
        SELECT 0 AS TOTALAA,
               SUM(DC.FNCOLOCABRUTA) AS TOTALSA,
               0 AS OBJETIVO,
               0 AS COLOCACIONBRUTA,
               DC.EsSucursal AS FIORIGEN,
               DC.FCSUCURSALORIGEN AS FcDescOrigen
               --DC.FCORIGEN AS FcDescOrigen
          FROM COLOCACION_AGRUPADA DC 
         WHERE 1=1
           AND DC.FDFECHA BETWEEN '2021-07-12' AND '2021-07-18'
           AND DC.ESTERRITORIO = 1
           AND DC.ESSUCURSAL IS NOT NULL
           --AND DC.FISUPERIOR = 12
           --AND DC.FICLASIFICACIONCTE IN(-1, 0, 1)
           --AND FIELEMENTO_PADRE = 10000007
           --AND DC.FIDIASEM BETWEEN 1 AND 7
      GROUP BY DC.EsSucursal ,
               DC.FCSUCURSALORIGEN
      		   --DC.FCORIGEN
  UNION
        SELECT 0 AS TOTALAA,
               0 AS TOTALSA,
               0 AS OBJETIVO,
               SUM(DC.FNCOLOCABRUTA) AS COLOCACIONBRUTA,
               DC.EsSucursal AS FIORIGEN,
               DC.FCSUCURSALORIGEN AS FcDescOrigen
               --DC.FCORIGEN AS FcDescOrigen
          FROM COLOCACION_AGRUPADA DC
         WHERE 1=1
           AND FDFECHA BETWEEN '2021-07-19' AND'2021-07-25'
           AND DC.ESTERRITORIO = 1
           AND DC.ESSUCURSAL IS NOT NULL
           --AND DC.FISUPERIOR = 12
           --AND DC.FICLASIFICACIONCTE IN(-1, 0, 1) 
           --AND DC.FIELEMENTO_PADRE = 10000007
           --AND DC.FIDIASEM BETWEEN 1 AND 7 
      GROUP BY DC.EsSucursal ,
               DC.FCSUCURSALORIGEN
      		   --DC.FCORIGEN
      		   
         UNION
        SELECT 0 AS TOTALAA,
               0 AS TOTALSA,
               SUM(DC.FNPLANCOMPROMISO) AS OBJETIVO,
               0 AS COLOCACIONBRUTA,
               DC.EsSucursal AS FIORIGEN,
               DC.FCSUCURSALORIGEN AS FcDescOrigen --Se habilita si lo que se muestra es el elemento padre
               --DC.FCDescOrigen AS FcDescOrigen --Se habilita si lo que se muestra es el elemento hijo
          FROM DETALLE_COLOCACION_PLAN DC
         WHERE 1=1
           AND DC.IDCC IS NOT NULL
	       AND DC.FDFECHA BETWEEN '2021-07-19' AND '2021-07-25'
	       --AND DC.FISUPERIOR = 12 --Se habilita si lo que se muestra es el elemnto hijo
           --AND DC.FICLASIFICACIONCTE IN(-1, 0, 1)
           --AND DCP.FIELEMENTO_PADRE = 10000007 
		   --AND DCP.FIDIASEM BETWEEN 1 AND 7 
	  GROUP BY DC.EsSucursal,
               DC.FCSUCURSALORIGEN   --Se habilita si lo que se muetra es el elemento padre|
	  		   --DC.FCDescOrigen --Se habilita si lo que se muestra es el elemento hijo
                           
     ) AS C
 GROUP BY C.FIORIGEN, 
 	      C.FcDescOrigen
 ORDER BY C.FIORIGEN;
         
---querys para el llenado de la grafica

----------------------------------------------------------------------
----------------A�o Anterior -- no tiene informacion solo hya info de 2 semanas
SELECT          CAST(ISNULL(SUM(FNCOLOCACIONIVA),0)AS DECIMAL(18,2)) AS FnMonto,
                DC.FiAnioSemana,
                'Total Colocaci�n Red �nica' as FcDescripcion
        FROM    DETALLE_COLOCACION DC
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
		AND ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND DC.FICONSIDERA =1
		AND  DC.FCUNIDADNEGOCIO !='238'
        AND     DC.FdFECHA BETWEEN '2019-08-01' AND '2020-07-26'
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1)  ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = DC.FIORIGEN  ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
       	GROUP BY DC.FiAnioSem,DC.FiSemana,FcDescripcion,FiAnioSemana
	   -- ORDER BY DC.FiSemana
UNION ALL 
----------------------------------------------------------------------
--------------Total Colocaci�n Red �nica
SELECT            CAST(ISNULL(SUM(FNCOLOCACIONIVA),0)AS DECIMAL(18,2)) AS FnMonto,
                DC.FiAnioSemana,
                'Total Colocaci�n Red �nica' as FcDescripcion
        FROM    DETALLE_COLOCACION DC
        WHERE   1=1AND     NOT DC.IDCC IS NULL
		AND ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND DC.FICONSIDERA =1
		AND  DC.FCUNIDADNEGOCIO !='238'
        AND     DC.FdFECHA BETWEEN '2019-08-01' AND '2021-08-01'
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = DC.FIORIGEN ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
       GROUP BY DC.FiAnioSem,DC.FiSemana,FcDescripcion,DC.FiAnioSemana
	   --ORDER BY DC.FiSemana  
UNION ALL  

-------------------------------------------------------------------
------------------------Miercoles - Domingo------------------------------
SELECT          CAST(ISNULL(SUM(FnPlanCompromiso),0)AS DECIMAL(18,2)) AS FnMonto, 
                DC.FiAnioSemana,
                'Miercoles - Domingo' as FcDescripcion
        FROM    DETALLE_COLOCACION_PLAN DC 
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
        AND     DC.FdFECHA BETWEEN '2019-07-29' AND '2021-08-01'
        AND     DC.FiDiaSem  BETWEEN 3 AND 7
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = DC.FiIdOrigenCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
        GROUP BY DC.FiAnioSem,DC.FiSemana,FcDescripcion,FiAnioSemana
	   --	ORDER BY DC.FiSemana 
UNION ALL 
-------------------------------------------------------------------
------------------------Lunes - Martes------------------------------
SELECT          cast(SUM(FnPlanCompromiso) as numeric(18,2))  AS FnMonto,
                DC.FiAnioSemana,
                'Objetivo Lunes - Martes' as FcDescripcion
        FROM    DETALLE_COLOCACION_PLAN DC
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
        AND     DC.FdFECHA BETWEEN '2019-07-29' AND '2021-08-01'
        AND     DC.FiDiaSem  BETWEEN 1 AND 2
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = DC.FiIdOrigenCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
        GROUP BY DC.FiAnioSem,DC.FiSemana,FcDescripcion,FiAnioSemana
	   --	ORDER BY DC.FiSemana
UNION  ALL                            
-------------------------------------------------------------------
------------------------'Objetivo Semanal------------------------------
SELECT          cast(SUM(FnPlanCompromiso) as numeric(18,2))  AS FnMonto,
                DC.FiAnioSemana,
                'Objetivo Semanal' as FcDescripcion
        FROM    DETALLE_COLOCACION_PLAN DC
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
        AND     DC.FdFECHA BETWEEN '2019-07-26' AND '2021-07-28'
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = DC.FiIdOrigenCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
        GROUP BY DC.FiAnioSem,DC.FiSemana,FcDescripcion,FiAnioSemana
	   --	ORDER BY DC.FiSemana
UNION ALL 	   	
---------------------------------------------------------
----------------Total Colocaci�n Bruta-------------------
SELECT     cast(SUM(FNCOLOCACIONIVA - FNCANCELACIONIVA) as numeric(18,2)) AS FnMonto,
                DC.FiAnioSemana,
                'Total Colocaci�n Bruta' as FcDescripcion
        FROM    DETALLE_COLOCACION DC
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
		AND 	ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND 	DC.FICONSIDERA =1
		AND  	DC.FCUNIDADNEGOCIO !='238'
        AND     DC.FdFECHA BETWEEN '2021-08-01' AND '2021-08-01'
       -- AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
       -- AND     DC.FISUPERIOR = DC.FIORIGEN ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
       	GROUP BY DC.FiAnioSem,DC.FiSemana,FcDescripcion,FiAnioSemana
	  -- 	ORDER BY DC.FiSemana
;        
----------------------------------------------------
----------------------------------------------------

------------------------------------------------------------------------------------
-----------query para llenar el cuadro de lado derecho de la grafica----------------
SELECT 
       cast(FnMonto as numeric(18,2)) , FcDescripcion , FiId
FROM (

SELECT          SUM(FNCOLOCACIONIVA) AS FnMonto,
                'Total Colocaci�n Red �nica' as FcDescripcion,1 AS FiId
        FROM    DETALLE_COLOCACION DC
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
		AND 	ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND 	DC.FICONSIDERA =1
		AND  	DC.FCUNIDADNEGOCIO !='238'
        AND     DC.FdFECHA BETWEEN '2019-08-01' AND '2020-07-26'
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = DC.FIORIGEN ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
       GROUP BY FcDescripcion
UNION ALL
SELECT          SUM(FNCOLOCACIONIVA) AS FnMonto,
                'Total Colocaci�n Red �nica' as FcDescripcion,2 AS FiId
        FROM    DETALLE_COLOCACION DC
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
		AND 	ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND 	DC.FICONSIDERA =1
		AND  	DC.FCUNIDADNEGOCIO !='238'
        AND     DC.FdFECHA BETWEEN '2019-08-01' AND '2021-08-01'
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = FIORIGEN ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
       GROUP BY FcDescripcion
UNION ALL
SELECT          cast(SUM(FnPlanCompromiso) as numeric(18,2))  AS FnMonto,
                'Miercoles - Domingo' as FcDescripcion,3 AS FiId
        FROM    DETALLE_COLOCACION_PLAN DC
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
        AND     DC.FdFECHA BETWEEN '2019-07-29' AND '2021-08-01'
        AND     DC.FiDiaSem  BETWEEN 3 AND 7
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = DC.FiIdOrigenCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
        GROUP BY  FcDescripcion

UNION ALL        
SELECT          cast(SUM(FnPlanCompromiso) as numeric(18,2))  AS FnMonto,
                'Objetivo Lunes - Martes' as FcDescripcion,4 AS FiId
        FROM    DETALLE_COLOCACION_PLAN DC 
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
        AND     DC.FdFECHA BETWEEN '2019-07-29' AND '2021-08-01'
        AND     DC.FiDiaSem  BETWEEN 1 AND 2
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = DC.FiIdOrigenCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
        GROUP BY FcDescripcion
UNION ALL                              
SELECT          cast(SUM(FnPlanCompromiso) as numeric(18,2))  AS FnMonto,
                'Objetivo Semanal' as FcDescripcion,5 AS FiId
        FROM    DETALLE_COLOCACION_PLAN DC
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
        AND     DC.FdFECHA BETWEEN '2021-07-26' AND '2021-08-01'
       -- AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
       -- AND     DC.FISUPERIOR = DC.FiIdOrigenCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
        GROUP BY FcDescripcion
UNION ALL
SELECT     cast(SUM(FNCOLOCACIONIVA - FNCANCELACIONIVA) as numeric(18,2)) AS FnMonto,
                'Total Colocaci�n Bruta' as FcDescripcion,6 AS FiId
        FROM    DETALLE_COLOCACION DC 
        WHERE   1=1
        AND     NOT DC.IDCC IS NULL
        AND ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND DC.FICONSIDERA =1
		AND  DC.FCUNIDADNEGOCIO !='238'
        AND     DC.FdFECHA BETWEEN '2021-07-26' AND '2021-08-01'
        --AND     DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        --AND     DC.FISUPERIOR = FIORIGEN ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
        
        GROUP BY FcDescripcion
   )AS C
  where c.FiId in(1,2,3,4,5,6)
   order by c.FiId 
   
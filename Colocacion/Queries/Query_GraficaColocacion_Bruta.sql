/**************************************** PARA PINTAR LA GRAFICA ***************************************/

----------------------------- A�O ANTERIOR ------------------------------
      SELECT CAST(ISNULL(SUM(FNCOLOCACIONIVA) - SUM(FNCANCELACIONIVA), 0) AS DECIMAL(18,2)) AS FnMonto, --FnA�oAnetior
             DC.FiAnioSemana,
             'A�o Anterior' AS FcDescripcion,
             1 AS FiID
        FROM DETALLE_COLOCACION DC
       WHERE 1=1
       AND NOT DC.IDCC IS NULL
		AND ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND DC.FICONSIDERA =1
		AND  DC.FCUNIDADNEGOCIO !='238'
 		AND DC.FDFECHA BETWEEN '2019-01-01' AND '2020-12-31' 
     	--AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
 		--AND DC.FISUPERIOR = DC.FIORIGEN    ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
     	
     GROUP BY DC.FiAnioSem, DC.FiSemana, FcDescripcion,FiAnioSemana
    -- ORDER BY p.anioSem,p.FiSemana
  UNION
----------------------------- TOTAL COLOCACIONBRUTA ------------------------------
      SELECT CAST(ISNULL(SUM(FNCOLOCACIONIVA) - SUM(FNCANCELACIONIVA), 0) AS DECIMAL(18,2)) AS FnMonto, --FnTotalColocacionBruta
             DC.FiAnioSemana,
             'Total Colocaci�n Bruta' AS FcDescripcion,
             2 AS FiID
        FROM DETALLE_COLOCACION DC
       WHERE 1=1AND NOT DC.IDCC IS NULL
		AND ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND DC.FICONSIDERA =1
		AND DC.FCUNIDADNEGOCIO !='238'
     	AND DC.FDFECHA BETWEEN '2019-01-01' AND '2021-12-31' 
		AND DC.FiDiaSem BETWEEN 1 AND 7
		--AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
		--AND DC.FISUPERIOR = DC.FIORIGEN   ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY DC.FiAnioSem, DC.FiSemana, FcDescripcion,FiAnioSemana
    -- ORDER BY p.ANIOSEM, p.SEMANA
  UNION
----------------------------- JUEVES - DOMINGO  ------------------------------
      SELECT CAST(ISNULL(SUM(FNPLANCOMPROMISO), 0) AS DECIMAL(18,2)) AS FnMonto, --JuevesDomingo
             DC.FiAnioSemana,
             'Jueves - Domingo' AS FcDescripcion,
             3 AS FiID
        FROM DETALLE_COLOCACION_PLAN DC
       WHERE 1=1
       	 AND NOT DC.IDCC IS NULL
         AND DC.FDFECHA BETWEEN '2019-01-01' AND '2021-12-31'
         AND DC.FiDiaSem BETWEEN 4 AND 7
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIIDORIGENCUC  ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY DC.FiAnioSemana, FcDescripcion
    -- ORDER BY p.ANIOSEM, p.SEMANA
  UNION 
----------------------------- LUNES - MIERCOLES  ------------------------------
      SELECT CAST(ISNULL(SUM(FNPLANCOMPROMISO), 0) AS DECIMAL(18,2)) AS FnMonto, --LunesMiercoles
             DC.FiAnioSemana,
             'Lunes - Miercoles' AS FcDescripcion,
             4 AS FiID
        FROM DETALLE_COLOCACION_PLAN DC
       WHERE 1=1
       AND NOT DC.IDCC IS NULL
         AND DC.FDFECHA BETWEEN '2019-01-01' AND '2021-12-31'
         AND DC.FiDiaSem BETWEEN 1 AND 3
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIIDORIGENCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY DC.FiAnioSemana, FcDescripcion
    -- ORDER BY p.ANIOSEM, p.SEMANA
  UNION
----------------------------- OBJETIVO SEMANAL  ------------------------------
      SELECT CAST(ISNULL(SUM(FNPLANCOMPROMISO), 0) AS DECIMAL(18,2)) AS FnMonto, --FnObjetivoSemanal
             DC.FiAnioSemana,
             'Objetivo Semanal' AS FcDescripcion,
             5 AS FiID
        FROM DETALLE_COLOCACION_PLAN DC 
       WHERE 1=1
         AND NOT DC.IDCC IS NULL
         AND DC.FDFECHA BETWEEN '2019-01-01' AND '2021-12-31'
         AND DC.FiDiaSem BETWEEN 1 AND 7
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIIDORIGENCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY DC.FiAnioSemana, FcDescripcion
    -- ORDER BY p.ANIOSEM, p.SEMANA
  UNION
----------------------------- COLOCACION NETA  ------------------------------
---------actualizar cuando haya cobranza para calcular la colocacion Neta
      SELECT CAST(ISNULL(SUM(FNCOLSINIVA), 0) AS DECIMAL(18,2)) AS FnMonto, --FnTotalColocacionNeta
             DC.FiAnioSemana,
             'Total Colocaci�n Neta' AS FcDescripcion,
             6 AS FiID
        FROM DETALLE_COLOCACION DC
       WHERE 1=1
       	AND NOT IDCC IS NULL
		AND ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND DC.FICONSIDERA =1  
		AND  DC.FCUNIDADNEGOCIO !='238'
        AND DC.FDFECHA BETWEEN '2019-01-01' AND '2021-12-31' 
        -- AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
        -- AND DC.FISUPERIOR = DC.FIORIGEN  ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY DC.FiAnioSemana, FcDescripcion
    ORDER BY FiID --p.ANIOSEM, p.SEMANAte
;


/**************************************** PARA PANEL DERECHO DE LA GRAFICA ***************************************/
----------------------------- A�O ANTERIOR ------------------------------
      SELECT CAST(ISNULL(SUM(FNCOLOCACIONIVA) - SUM(FNCANCELACIONIVA), 0) AS DECIMAL(18,2)) AS FnMonto, --FnA�oAnetior
             'A�o Anterior' AS FcDescripcion,
             1 AS FiID
        FROM DETALLE_COLOCACION DC
       WHERE 1=1
       AND NOT DC.IDCC IS NULL
		AND ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND DC.FICONSIDERA =1
		AND  DC.FCUNIDADNEGOCIO !='238'
         AND DC.FDFECHA BETWEEN '2020-07-26' AND '2020-08-01'
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIORIGEN ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY FcDescripcion
  UNION
----------------------------- TOTAL COLOCACIONBRUTA ------------------------------
      SELECT CAST(ISNULL(SUM(FNCOLOCACIONIVA) - SUM(FNCANCELACIONIVA), 0) AS DECIMAL(18,2)) AS FnMonto, --FnTotalColocacionBruta
             'Total Colocaci�n Bruta' AS FcDescripcion,
             2 AS FiID
        FROM DETALLE_COLOCACION DC
       WHERE 1=1
       AND NOT DC.IDCC IS NULL
		AND ISNULL(FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND DC.FICONSIDERA =1
		AND  DC.FCUNIDADNEGOCIO !='238'
         AND DC.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01'
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIORIGEN ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY FcDescripcion
  UNION 
----------------------------- JUEVES - DOMINGO  ------------------------------
      SELECT CAST(ISNULL(SUM(FNPLANCOMPROMISO), 0) AS DECIMAL(18,2)) AS FnMonto, --JuevesDomingo
             'Jueves - Domingo' AS FcDescripcion,
             3 AS FiID
        FROM DETALLE_COLOCACION_PLAN DC
       WHERE 1=1
       AND NOT IDCC IS NULL  
       AND DC.FDFECHA BETWEEN '2021-07-22' AND '2021-07-25'
         AND DC.FiDiaSem BETWEEN 4 AND 7
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIIDORIGENCUC  ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY FcDescripcion
  UNION 
----------------------------- LUNES - MIERCOLES  ------------------------------
      SELECT CAST(ISNULL(SUM(FNPLANCOMPROMISO), 0) AS DECIMAL(18,2)) AS FnMonto, --LunesMiercoles
             'Lunes - Miercoles' AS FcDescripcion,
             4 AS FiID
        FROM DETALLE_COLOCACION_PLAN DC
       WHERE 1=1
       	 AND NOT DC.IDCC IS NULL
         AND DC.FDFECHA BETWEEN '2021-07-26' AND '2021-07-28'
         AND DC.FiDiaSem BETWEEN 1 AND 1
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIIDORIGENCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY FcDescripcion
  UNION
----------------------------- OBJETIVO SEMANAL  ------------------------------
      SELECT CAST(ISNULL(SUM(FNPLANCOMPROMISO), 0) AS DECIMAL(18,2)) AS FnMonto, --FnObjetivoSemanal
             'Objetivo Semanal' AS FcDescripcion,
             5 AS FiID
        FROM DETALLE_COLOCACION_PLAN DC
       WHERE 1=1
       	 AND NOT DC.IDCC IS NULL
         AND DC.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01'
         AND DC.FiDiaSem BETWEEN 1 AND 7
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIIDORIGENCUC ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
         GROUP BY FcDescripcion
    -- ORDER BY p.ANIOSEM, p.SEMANA
  UNION
----------------------------- COLOCACION NETA  ------------------------------
      SELECT CAST(ISNULL(SUM(FNCOLSINIVA), 0) AS DECIMAL(18,2)) AS FnMonto, --FnTotalColocacionNeta
             'Total Colocaci�n Neta' AS FcDescripcion,
             6 AS FiID
        FROM DETALLE_COLOCACION DC 
       WHERE 1=1
       AND NOT DC.IDCC IS NULL
         AND ISNULL(DC.FIREPROGRAMACION,0) NOT IN (1,2,4)
		AND  DC.FICONSIDERA =1
		AND  DC.FCUNIDADNEGOCIO !='238'
         AND DC.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01'
         --AND DC.FICLASIFICACIONCTE IN (-1, 0, 1) ---se habilitara cuando se busque el filtro por tipo de cliente
         --AND DC.FISUPERIOR = DC.FIORIGEN  ---se habilitara cuando se busque informacion en el tabular de originacion y se agrega el parametro
    GROUP BY FcDescripcion
    ORDER BY FiID --p.ANIOSEM, p.SEMANAte
;
























--    EXPLAIN
    SELECT SUM(TOTALAA) as AnioAnt,
           SUM(TOTALSA) AS SemAnt,
           SUM(OBJETIVO) AS Objetivo,
           SUM(COLOCACIONIVA) AS 'Real',
           SUM(COLOCACIONIVA) - SUM(OBJETIVO) AS VsObjetivo,
           SUM(COLOCACIONIVA)-SUM(TOTALAA) AS VsAnioAnt , 
           SUM(COLOCACIONIVA)-SUM(TOTALSA) AS VsSemAnt,
           DC.IDDIA Id,
           DC.DESCDIASEM Descripcion
      FROM (
                SELECT DC.FIDIASEM IDDIA,
                       DC.FCDESCDIASEM DESCDIASEM,
                       0 TOTALAA,
                       0 TOTALSA,
                       0 OBJETIVO,
                       0 COLOCACIONIVA 
                  FROM DETALLE_COLOCACION DC
                 WHERE 1=1
                   AND DC.IDCC IS NOT NULL
                   AND ISNULL(DC.FIREPROGRAMACION, 0) NOT IN (1,2,4)
                   AND DC.FICONSIDERA = 1
                   AND DC.FCUNIDADNEGOCIO != '238'
                   AND DC.FDFECHA BETWEEN '2020-07-26' AND '2020-07-26'
--                   AND DC.FICLASIFICACIONCTE IN (0,1,-1) --Condici�n que debe integrarse solo si se seleccion� un tipo de cliente desde el dropdownlist
                   --AND DC.FIELEMENTO_PADRE = 10000007 --Condici�n que se enlaza en caso de traer el par�metro para formato
                   --AND DC.FIDIASEM BETWEEN 1 AND 7 -- par�metro del tabular d�a
                   --AND DC.FISUPERIOR = DC.FIORIGEN --par�metro tabular originaci�n FIORIGEN
              GROUP BY DC.FIDIASEM,
                       DC.FCDESCDIASEM
                 UNION 
                SELECT DC.FIDIASEM IDDIA,
                       DC.FCDESCDIASEM DESCDIASEM,
                       0 TOTALAA, 
                       ISNULL(SUM(DC.FNCOLOCACIONIVA), 0) TOTALSA, 
                       0 OBJETIVO,
                       0 COLOCACIONIVA
                  FROM DETALLE_COLOCACION DC
                 WHERE 1=1
                   AND DC.IDCC IS NOT NULL
                   AND ISNULL(DC.FIREPROGRAMACION, 0) NOT IN (1,2,4)
                   AND DC.FICONSIDERA = 1
                   AND DC.FCUNIDADNEGOCIO != '238'
                   AND DC.FDFECHA BETWEEN '2021-07-12' AND '2021-07-18'
                   --AND DC.FICLASIFICACIONCTE IN (0,1,-1) 
                   --AND DC.FIELEMENTO_PADRE = 10000007 --Condici�n que se enlaza en caso de traer el par�metro para formato
                   --AND DC.FIDIASEM BETWEEN 1 AND 7 -- par�metro del tabular d�a
                   --AND DC.FISUPERIOR = DC.FIORIGEN --par�metro tabular originaci�n FIORIGEN
              GROUP BY DC.FIDIASEM,
                       DC.FCDESCDIASEM
                 UNION 
                SELECT DC.FIDIASEM IDDIA,
                       DC.FCDESCDIASEM DESCDIASEM,
                       0 TOTALAA,
                       0 TOTALSA,  
                       0 OBJETIVO,
                       ISNULL(SUM(DC.FNCOLOCACIONIVA), 0) COLOCACIONIVA 
                  FROM DETALLE_COLOCACION DC
                 WHERE 1=1
                   AND DC.IDCC IS NOT NULL
                   AND ISNULL(DC.FIREPROGRAMACION, 0) NOT IN (1,2,4)
                   AND DC.FICONSIDERA = 1
                   AND DC.FCUNIDADNEGOCIO != '238'
                   AND DC.FDFECHA BETWEEN '2021-07-19' AND '2021-07-25'
                   --AND DC.FICLASIFICACIONCTE IN (0,1,-1) 
                   --AND DC.FIELEMENTO_PADRE = 10000007 --Condici�n que se enlaza en caso de traer el par�metro para formato
                   --AND DC.FIDIASEM BETWEEN 1 AND 7 -- par�metro del tabular d�a
                   --AND DC.FISUPERIOR = DC.FIORIGEN --par�metro tabular originaci�n FIORIGEN
              GROUP BY DC.FIDIASEM,
                       DC.FCDESCDIASEM
                 UNION
                SELECT DC.FIDIASEM IDDIA,
                       DC.FCDESCDIASEM DESCDIASEM,
                       0 TOTALAA,
                       0 TOTALSA,  
                       ISNULL(SUM(DC.FNPLANCOMPROMISO), 0) OBJETIVO,
                       0 COLOCACIONIVA 
                  FROM DETALLE_COLOCACION_PLAN DC
                 WHERE 1=1
                   AND DC.IDCC IS NOT NULL
                   AND DC.FDFECHA BETWEEN '2021-07-19' AND '2021-07-25'
                   --AND DC.FICLASIFICACIONCTE IN (0,1,-1)
                   --AND DC.FIELEMENTO_PADRE = 10000007 
				   --AND DC.FIDIASEM BETWEEN 1 AND 7 
                   --AND DC.FISUPERIOR = DC.FiIdOrigenCUC 
              GROUP BY DC.FIDIASEM,
                       DC.FCDESCDIASEM
                       
           ) DC
  GROUP BY DC.DESCDIASEM, DC.IDDIA
  ORDER BY DC.IDDIA


  

  
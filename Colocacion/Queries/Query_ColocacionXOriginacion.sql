--EXPLAIN
SELECT 
       SUM(TOTALAA)         AS AnioAnt,
       SUM(TOTALSA)         AS SemAnt,
       SUM(OBJETIVO)        AS Objetivo,
       SUM(CAPITAL_ACTUAL)  AS 'Real',
       SUM(CAPITAL_ACTUAL) - SUM(OBJETIVO) AS VsObjetivo,
       SUM(CAPITAL_ACTUAL) - SUM(TOTALSA) AS VsSemAnt, 
       SUM(CAPITAL_ACTUAL) - SUM(TOTALAA) AS VsAnioAnt, 
       FIORIGEN             AS Id,
       FcDescOrigen         AS Descripcion
FROM (
        SELECT SUM(DC.FNCOLOCACIONIVA) AS TOTALAA,
               0 AS TOTALSA,
               0 AS OBJETIVO,
               0 AS CAPITAL_ACTUAL,
               DC.EsSucursal AS FIORIGEN,
               DC.FCSUCURSALORIGEN AS FcDescOrigen
          FROM DETALLE_COLOCACION DC
         WHERE 1 = 1 
           AND DC.IDCC IS NOT NULL
           AND ISNULL(DC.FIREPROGRAMACION, 0) NOT IN (1,2,4)
           AND DC.FICONSIDERA = 1
           AND DC.FCUNIDADNEGOCIO != '238'
           AND DC.FDFECHA BETWEEN '2020-07-26' AND '2020-07-26'
           AND DC.FISUPERIOR = DC.FIORIGEN -- DC.FIORIGEN se debe sustituir por el ID del origen seleccionado en el tabular
           --AND DC.FICLASIFICACIONCTE IN(-1, 0, 1) --se agrega a la consulta solo si se seleccion� alg�n elemento del dropdownlist
           --AND FIELEMENTO_PADRE = 10000007 --Condici�n que se enlaza en caso de traer el par�metro para formato
           --AND DC.FIDIASEM BETWEEN 1 AND 7 -- par�metro del tabular d�a
      GROUP BY DC.EsSucursal,
               DC.FCSUCURSALORIGEN 
  UNION
        SELECT 0 AS TOTALAA,
               SUM(DC.FNCOLOCACIONIVA) AS TOTALSA,
               0 AS OBJETIVO,
               0 AS CAPITAL_ACTUAL,
               DC.EsSucursal AS FIORIGEN,
               DC.FCSUCURSALORIGEN AS FcDescOrigen
          FROM DETALLE_COLOCACION DC 
         WHERE 1=1
           AND DC.IDCC IS NOT NULL
           AND ISNULL(DC.FIREPROGRAMACION, 0) NOT IN (1,2,4)
           AND DC.FICONSIDERA = 1
           AND DC.FCUNIDADNEGOCIO != '238'
           AND DC.FDFECHA BETWEEN '2021-07-25' AND '2021-07-25'
           AND DC.FISUPERIOR = DC.FIORIGEN 
           --AND DC.FICLASIFICACIONCTE IN(-1, 0, 1)
           --AND FIELEMENTO_PADRE = 10000007
           --AND DC.FIDIASEM BETWEEN 1 AND 7
      GROUP BY DC.EsSucursal ,
               DC.FCSUCURSALORIGEN
  UNION
        SELECT 0 AS TOTALAA,
               0 AS TOTALSA,
               0 AS OBJETIVO,
               SUM(DC.FNCOLOCACIONIVA) AS CAPITAL_ACTUAL,
               DC.EsSucursal AS FIORIGEN,
               DC.FCSUCURSALORIGEN AS FcDescOrigen
          FROM DETALLE_COLOCACION DC
         WHERE 1=1
           AND DC.IDCC IS NOT NULL
           AND ISNULL(DC.FIREPROGRAMACION, 0) NOT IN (1,2,4)
           AND DC.FICONSIDERA = 1
           AND DC.FCUNIDADNEGOCIO != '238'
           AND FDFECHA BETWEEN '2021-08-01' AND '2021-08-01'
           AND DC.FISUPERIOR = DC.FIORIGEN 
           --AND DC.FICLASIFICACIONCTE IN(-1, 0, 1) 
           --AND DC.FIELEMENTO_PADRE = 10000007
           --AND DC.FIDIASEM BETWEEN 1 AND 7 
      GROUP BY DC.EsSucursal ,
               DC.FCSUCURSALORIGEN
         UNION
        SELECT 0 AS TOTALAA,
               0 AS TOTALSA,
               SUM(DC.FNPLANCOMPROMISO) AS OBJETIVO,
               0 AS CAPITAL_ACTUAL,
               DC.EsSucursal AS FIORIGEN,
               DC.FCSUCURSALORIGEN AS FcDescOrigen
          FROM DETALLE_COLOCACION_PLAN DC
         WHERE 1=1
           AND DC.IDCC IS NOT NULL
	       AND DC.FDFECHA BETWEEN '2021-08-01' AND '2021-08-01'
	       AND DC.FISUPERIOR = DC.FiIdOrigenCUC 
           --AND DC.FICLASIFICACIONCTE IN(-1, 0, 1)
           --AND DCP.FIELEMENTO_PADRE = 10000007 
		   --AND DCP.FIDIASEM BETWEEN 1 AND 7 
	  GROUP BY DC.EsSucursal,
               DC.FCSUCURSALORIGEN               
     ) AS C
 GROUP BY C.FIORIGEN, 
 	      C.FcDescOrigen
 ORDER BY C.FIORIGEN;

     
     
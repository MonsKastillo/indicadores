--COLOCACION_AGRUPADA

SELECT 
	   SUM(TOTALAA) 	AS AnioAnt	 ,
	   SUM(TOTALSA)         AS SemAnt,
	   0       AS Objetivo,
       SUM(TOTAL_REAL)  AS 'Real',
       PRODUCTO,
       SUM(TOTAL_REAL) - SUM(OBJETIVO) AS VsObjetivo,
       SUM(TOTAL_REAL) - SUM(TOTALSA) AS VsSemAnt, 
       SUM(TOTAL_REAL) - SUM(TOTALAA) AS VsAnioAnt, 
       IDTIPOTOTAL
FROM
(
-------------------------------------------------------------------------
--------------------------AÑO ANTERIROR-----------------------------------

SELECT  CAST(ISNULL(SUM(FNENGANCHE),0) AS DECIMAL(18,2)) AS TOTALAA,
		0 TOTALSA,
		0 		TOTAL_REAL,
		1 AS 	IDTIPOTOTAL,
		'(-) Enganche' AS   PRODUCTO
        FROM    COLOCACION_AGRUPADA 
        WHERE   1 = 1 
        AND     FDFECHA BETWEEN '2020-07-13' AND '2020-07-19'
        AND     FiDiaSem  BETWEEN 1 AND 7
       
        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN
UNION ALL 
        SELECT CAST(ISNULL(SUM(FNCOBCAPITAL),0)  AS DECIMAL(18,2)) AS TOTALAA,
        0 TOTALSA,
		0 		TOTAL_REAL,
		2 AS 	IDTIPOTOTAL,
		'(-) Liquidación Anticipada' as PRODUCTO
        FROM    COLOCACION_AGRUPADA 
        WHERE   1 = 1 
       	AND     FDFECHA BETWEEN '2020-07-13' AND '2020-07-19'
        AND     FiDiaSem  BETWEEN 1 AND 7

        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN
UNION ALL       
         SELECT  CAST(ISNULL(SUM(FNCOBLIQUID),0)  AS DECIMAL(18,2))AS TOTALAA,
         0 TOTALSA,
		0 		TOTAL_REAL,
		3 AS 	IDTIPOTOTAL,
		'(-) Liquidación Por Renovación' as PRODUCTO
		FROM    COLOCACION_AGRUPADA 		
        WHERE   1 = 1 
        AND     FDFECHA BETWEEN '2020-07-13' AND '2020-07-19'
        AND     FiDiaSem  BETWEEN 1 AND 7
    
        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN
UNION ALL 

-------------------------------------------------------------------------
--------------------------SEMANA ANTERIROR-------------------------------

SELECT  0 TOTALAA,
		CAST(ISNULL(SUM(FNENGANCHE),0) AS DECIMAL(18,2)) AS TOTALSA,
		0 		TOTAL_REAL,
		1 AS 	IDTIPOTOTAL,
		'(-) Enganche' AS   PRODUCTO
        FROM    COLOCACION_AGRUPADA 
        WHERE   1 = 1 
--        AND     FDFECHA BETWEEN '2020-07-13' AND '2020-07-19'
        AND     FDFECHA BETWEEN '2021-07-12' AND '2021-07-18'
        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN
UNION ALL 
        SELECT  0 TOTALAA,
        CAST(ISNULL(SUM(FNCOBCAPITAL),0)  AS DECIMAL(18,2)) AS TOTALSA,
		0 		TOTAL_REAL,
		2 AS 	IDTIPOTOTAL,
		'(-) Liquidación Anticipada' as PRODUCTO
        FROM    COLOCACION_AGRUPADA 
        WHERE   1 = 1 
       	--AND     FDFECHA BETWEEN '2020-07-13' AND '2020-07-19'
        AND     FiDiaSem  BETWEEN 1 AND 7
        AND     FDFECHA BETWEEN '2021-07-12' AND '2021-07-18'
        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN
UNION ALL       
         SELECT  0 TOTALAA,
         CAST(ISNULL(SUM(FNCOBLIQUID),0)  AS DECIMAL(18,2))AS TOTALSA,
		0 		TOTAL_REAL,
		3 AS 	IDTIPOTOTAL,
		'(-) Liquidación Por Renovación' as PRODUCTO
		FROM    COLOCACION_AGRUPADA 		
        WHERE   1 = 1 
        --AND     FDFECHA BETWEEN '2020-07-13' AND '2020-07-19'
        AND     FDFECHA BETWEEN '2021-07-12' AND '2021-07-18'
        AND     FiDiaSem  BETWEEN 1 AND 7
        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN
UNION ALL 
-------------------------------------------------------------------------
--------------------------REAL-----------------------------------
SELECT 	0 AS TOTALAA,
		0 TOTALSA,
		CAST(ISNULL(SUM(FNENGANCHE),0) AS DECIMAL(18,2)) AS TOTAL_REAL,
		1 AS 	IDTIPOTOTAL,
		'(-) Enganche' AS   PRODUCTO
        FROM    COLOCACION_AGRUPADA 
        WHERE   1 = 1 
        AND     FDFECHA BETWEEN '2021-07-19' AND '2021-07-25'
        AND     FiDiaSem  BETWEEN 1 AND 7
        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN
UNION ALL 
        SELECT  0 AS TOTALAA,
        		0 TOTALSA,
               CAST(ISNULL(SUM(FNCOBCAPITAL),0)  AS DECIMAL(18,2)) AS TOTAL_REAL,
		2 AS 	IDTIPOTOTAL,
		'(-) Liquidación Anticipada' as PRODUCTO
        FROM    COLOCACION_AGRUPADA 
        WHERE   1 = 1 
       AND     FDFECHA BETWEEN '2021-07-19' AND '2021-07-25'
        AND     FiDiaSem  BETWEEN 1 AND 7
        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN
UNION ALL       
         SELECT 0 AS TOTALAA,
         0 TOTALSA,
         CAST(ISNULL(SUM(FNCOBLIQUID),0)  AS DECIMAL(18,2))AS TOTAL_REAL,
        3 AS 	IDTIPOTOTAL,
		'(-) Liquidación Por Renovación' as PRODUCTO
		FROM    COLOCACION_AGRUPADA 		
        WHERE   1 = 1 
        AND     FDFECHA BETWEEN '2021-07-19' AND '2021-07-25'
        AND     FiDiaSem  BETWEEN 1 AND 7
        --AND     FICLASIFICACIONCTE IN(-1, 0, 1)
        --AND FIELEMENTO_PADRE = 10000002
        --AND     FISUPERIOR = FIORIGEN

) AS A  
GROUP BY A.PRODUCTO,A.IDTIPOTOTAL
ORDER BY A.IDTIPOTOTAL

--SELECT * from DimPeriodos dp where fecha = '2021-07-19'
--SELECT * from DimPeriodos dp where semana = 28 and anioSem = 2021 order by diaSem 
--   EXPLAIN
    SELECT SUM(TOTALAA) as AnioAnt,
           SUM(TOTALSA) AS SemAnt,
           SUM(OBJETIVO) AS Objetivo,
           SUM(COLOCACIONBRUTA) AS 'Real',
           SUM(COLOCACIONBRUTA) - SUM(OBJETIVO) AS VsObjetivo,
           SUM(COLOCACIONBRUTA)-SUM(TOTALAA) AS VsAnioAnt , 
           SUM(COLOCACIONBRUTA)-SUM(TOTALSA) AS VsSemAnt, 
           DC.FIELEMENTO_PADRE Id,   
           DC.ELEMENTO Descripcion
      FROM (
                SELECT 
                       DC.FIELEMENTO_PADRE,
                       --DC.FCELEMENTO ELEMENTO, --Se concatena si viene un par�metro de FIELEMENTO_PADRE
                       DC.FCELEMENTO_PADRE ELEMENTO, --Se concatena si no viene un par�metro de FIELEMENTO_PADRE
                       0 TOTALAA,
                       0 TOTALSA,
                       0 OBJETIVO,
                       0 COLOCACIONBRUTA 
                  FROM COLOCACION_AGRUPADA DC
                 WHERE 1=1
                  AND DC.FDFECHA BETWEEN '2020-07-26' AND '2020-07-26'
                   AND DC.ESTERRITORIO = 1
                   AND DC.FIELEMENTO_PADRE NOT IN (10000008, 10001004, 10001002, 10001007)
                   --AND FIELEMENTO_PADRE = 10000002 --Condición que se enlaza en caso de seleccionar un elemento en el tabular
                   --AND DC.FICLASIFICACIONCTE IN (0,1,-1) --Se habilita solo en caso de que se haya seleccionado un tipo de cliente
                   --AND DC.FIDIASEM BETWEEN 1 AND 7 -- parámetro del tabular día
                   --AND DC.FISUPERIOR = DC.FIORIGEN --parámetro tabular originación FIORIGEN
              GROUP BY 
                       --DC.FCELEMENTO, --Se concatena si viene un par�metro de FIELEMENTO_PADRE
                       DC.FCELEMENTO_PADRE, --Se concatena si no viene un par�metro de FIELEMENTO_PADRE
                       DC.FIELEMENTO_PADRE
                 UNION 
                SELECT 
                       DC.FIELEMENTO_PADRE,
                       --DC.FCELEMENTO ELEMENTO, 
                       DC.FCELEMENTO_PADRE ELEMENTO, 
                       0 TOTALAA, 
                       SUM(DC.FNCOLOCABRUTA) TOTALSA ,
                       0 OBJETIVO,
                       0 COLOCACIONBRUTA
                  FROM COLOCACION_AGRUPADA DC
                 WHERE 1=1
                   AND DC.FDFECHA BETWEEN '2021-07-19' AND '2021-07-25'
                   AND DC.ESTERRITORIO = 1
                   AND DC.FIELEMENTO_PADRE NOT IN (10000008, 10001004, 10001002, 10001007)
                   --AND FIELEMENTO_PADRE = 10000002 
                   --AND DC.FICLASIFICACIONCTE IN (0,1,-1)
				   --AND DC.FIDIASEM BETWEEN 1 AND 7 
                   --AND DC.FISUPERIOR = DC.FIORIGEN 
              GROUP BY 
                      --DC.FCELEMENTO, 
                      DC.FCELEMENTO_PADRE,
                      DC.FIELEMENTO_PADRE
                 UNION 
                SELECT DC.FIELEMENTO_PADRE,
                       --DC.FCELEMENTO ELEMENTO,
                       DC.FCELEMENTO_PADRE ELEMENTO, 
                       0 TOTALAA,
                       0 TOTALSA,  
                       0 OBJETIVO,
                       SUM(DC.FNCOLOCABRUTA) COLOCACIONBRUTA 
                  FROM COLOCACION_AGRUPADA DC
                 WHERE 1=1
                   AND DC.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01'
                   AND DC.ESTERRITORIO = 1
                   AND DC.FIELEMENTO_PADRE NOT IN (10000008, 10001004, 10001002, 10001007)
                   --AND FIELEMENTO_PADRE = 10000002
                   --AND DC.FICLASIFICACIONCTE IN (0,1,-1)
				   --AND DC.FIDIASEM BETWEEN 1 AND 7 
                   --AND DC.FISUPERIOR = DC.FIORIGEN 
              GROUP BY 
              		   --DC.FCELEMENTO, 
                       DC.FCELEMENTO_PADRE,
                       DC.FIELEMENTO_PADRE
                       
                 UNION
                SELECT DC.FIELEMENTO_PADRE,
                       --DC.FCELEMENTO ELEMENTO,
                       DC.FCELEMENTO_PADRE ELEMENTO,
                       0 TOTALAA,
                       0 TOTALSA,  
                       SUM(DC.FNPLANCOMPROMISO) OBJETIVO,
                       0 COLOCACIONBRUTA 
                  FROM DETALLE_COLOCACION_PLAN DC
                 WHERE 1=1
                   AND DC.IDCC IS NOT NULL
                   AND DC.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01'                   
                   AND DC.FIELEMENTO_PADRE NOT IN (10000008, 10001004, 10001002, 10001007)
                   --AND DC.FIELEMENTO_PADRE = 10000002
                   --AND DC.FICLASIFICACIONCTE IN (0,1,-1)
				   --AND DC.FIDIASEM BETWEEN 1 AND 7 
                   --AND DC.FISUPERIOR = DC.FiIdOrigenCUC 
              GROUP BY 
                       --DC.FCELEMENTO,
                       DC.FCELEMENTO_PADRE,
                       DC.FIELEMENTO_PADRE
           ) DC
  GROUP BY DC.FIELEMENTO_PADRE,   
           DC.ELEMENTO
  ORDER BY DC.ELEMENTO
  
  
--  select distinct FCELEMENTO_PADRE from CAT_FORMATOU cf 
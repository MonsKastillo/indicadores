/**************CONSULTA TABULADOR PRODUCTO**********
autor:          AGC
creacion:       24 AGOSTO 2021
VERSION:        4.0
Comentarios:    26-08-2021  Se agrega diaSemana para filtro por dia
                30-08-2021  se agrega filtros (IDCC,FIREPROGRAMACION,FCUNIDADNEGOCIO)
                 1-09-2021  Se modifica consulta y genera tabla de EstructuraProductos
                 2-09-2021  Se ordena filtros de acuerdo a estructura establecida
                 7-09-2021  Se cambia tabla detalle_colocacion a Colocacion_agrupada
                 14-09-2021 Se agrega filtros de ESTERRITORIO=1 
********************************************/

SELECT SUM(TOTALAA) AnioAnt, SUM(TOTALSA) SemAnt, cast(SUM(OBJETIVO) as numeric(18,2)) Objetivo,SUM(TOTALREAL) Real,FINIVEL2PROD Id,FCNIVEL2PROD Descripcion,
        SUM(TOTALREAL)-CAST(SUM(OBJETIVO) AS NUMERIC(18,2)) vsObjetivo, SUM(TOTALREAL)-SUM(TOTALSA) vsSemAnt, SUM(TOTALREAL)-SUM(TOTALAA) vsAnioAnt,NIVEL
 FROM (
  --------------------------------------------------- --TOTALAA
SELECT ISNULL(SUM(FnColocaBruta),0) TOTALAA,
       0 TOTALSA,
       0 OBJETIVO,
       0 TOTALREAL,
       FCNIVEL1PROD,
       FCNIVEL2PROD, 
       FINIVEL1PROD,
       FINIVEL2PROD,
       p.NIVEL 
  FROM COLOCACION_AGRUPADA c 
 INNER JOIN EstructuraProductos p 
    ON c.finivel2prod=p.idProducto 
   AND p.Visible =1
 WHERE 1=1
   AND c.FDFECHA BETWEEN  '2020-07-20' AND '2020-07-26'     ---semana del a�o pasado
   AND c.ESTERRITORIO = 1
   --AND c.FidiaSem BETWEEN 1 AND 7
   --AND c.FIELEMENTO_PADRE = 10000007 --Condicion que se enlaza en caso de traer el parametro para formato
   --AND C.FISUPERIOR = C.FIORIGEN --DEPENDE DEL TABULAR DE ORIGINACION
   --AND C.FICLASIFICACIONCTE IN(-1, 0, 1) --DEPENDE DEL FILTRO DE ....
 GROUP BY FCNIVEL1PROD, FCNIVEL2PROD, FINIVEL1PROD, FINIVEL2PROD,NIVEL
 UNION
SELECT ISNULL(SUM(FnColocaBruta),0) TOTALAA,
       0 TOTALSA,
       0 OBJETIVO,
       0 TOTALREAL,
       FCNIVEL1PROD, 
       FCNIVEL1PROD,
       FINIVEL1PROD, 
       FINIVEL1PROD,
       p.NIVEL 
  FROM COLOCACION_AGRUPADA c
 INNER JOIN EstructuraProductos p 
    ON c.finivel1prod=p.idProducto 
   AND p.Visible =1
 WHERE 1=1 
   AND c.FDFECHA BETWEEN  '2020-07-20' AND '2020-07-26'  ---semana del a�o pasado
   AND c.ESTERRITORIO = 1
   --AND c.FidiaSem BETWEEN 1 AND 7
   --AND C.FIELEMENTO_PADRE = 10000007 --Condicion que se enlaza en caso de traer el parametro para formato
   --AND C.FISUPERIOR = C.FIORIGEN --DEPENDE DEL TABULAR DE ORIGINACION
   --AND C.FICLASIFICACIONCTE IN(-1, 0, 1) --DEPENDE DEL FILTRO DE ....
 GROUP BY FCNIVEL1PROD, FINIVEL1PROD ,NIVEL
 UNION 
------------------------------------------------------ ---TOTALSA
SELECT 0 TOTALAA,
       sum(FnColocaBruta) TOTALSA,
       0 OBJETIVO,
       0 TOTALREAL,
       FCNIVEL1PROD,
       FCNIVEL2PROD, 
       FINIVEL1PROD,
       FINIVEL2PROD,
       p.NIVEL 
  FROM COLOCACION_AGRUPADA c 
 INNER JOIN EstructuraProductos p 
    ON c.finivel2prod=p.idProducto 
   AND p.Visible =1
 WHERE 1=1 
   AND c.FDFECHA BETWEEN  '2021-07-19' AND '2021-07-25'  --semana anterior
   AND c.ESTERRITORIO = 1
   --AND c.FidiaSem BETWEEN 1 AND 7
   --AND C.FIELEMENTO_PADRE = 10000007 --Condicion que se enlaza en caso de traer el parametro para formato
   --AND C.FISUPERIOR = C.FIORIGEN --DEPENDE DEL TABULAR DE ORIGINACION
   --AND C.FICLASIFICACIONCTE IN(-1, 0, 1) --DEPENDE DEL FILTRO DE ....
 GROUP BY FCNIVEL1PROD, FCNIVEL2PROD, FINIVEL1PROD, FINIVEL2PROD, NIVEL
 UNION
SELECT 0 TOTALAA,
       sum(FnColocaBruta)TOTALSA,
       0 OBJETIVO,
       0 TOTALREAL,
       FCNIVEL1PROD, 
       FCNIVEL1PROD,
       FINIVEL1PROD, 
       FINIVEL1PROD,
       p.NIVEL 
  FROM COLOCACION_AGRUPADA c 
 INNER JOIN EstructuraProductos p 
    ON c.finivel1prod=p.idProducto 
   AND p.Visible =1
 WHERE 1=1 
   AND c.FDFECHA BETWEEN  '2021-07-19' AND '2021-07-25'  --semana anterior
   AND c.ESTERRITORIO = 1
   --AND c.FidiaSem BETWEEN 1 AND 7
   --AND c.FIELEMENTO_PADRE = 10000007 --Condicion que se enlaza en caso de traer el parametro para formato
   --AND C.FISUPERIOR = C.FIORIGEN --DEPENDE DEL TABULAR DE ORIGINACION
   --AND C.FICLASIFICACIONCTE IN(-1, 0, 1) --DEPENDE DEL FILTRO DE ....
 GROUP BY FCNIVEL1PROD, FINIVEL1PROD, NIVEL
 UNION 
  --------------------------------------------------------TOTAL OBJETIVO
 SELECT 0 TOTALAA,
       0 TOTALSA,
       ISNULL(SUM(FNPLANCOMPROMISO),0)  OBJETIVO,
       0 TOTALREAL,
       FCNIVEL1PROD,
       FCNIVEL2PROD, 
       FINIVEL1PROD,
       FINIVEL2PROD,
       p.NIVEL 
  FROM DETALLE_COLOCACION_PLAN c 
 INNER JOIN EstructuraProductos p 
    ON c.finivel2prod=p.idProducto 
   AND p.Visible =1
 WHERE 1=1  
   and c.IDCC is not null
   AND c.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01' --semana actual
   --AND c.FidiaSem BETWEEN 1 AND 7
   --AND c.FIELEMENTO_PADRE = 10000007 --Condicion que se enlaza en caso de traer el parametro para formato
   --AND c.FISUPERIOR =c.FiIdOrigenCUC  --DEPENDE DEL TABULAR DE ORIGINACION
   --AND c.FICLASIFICACIONCTE IN (-1, 0, 1) --DEPENDE DEL FILTRO DE ....
 GROUP BY FCNIVEL1PROD, FCNIVEL2PROD, FINIVEL1PROD, FINIVEL2PROD, NIVEL
 UNION
SELECT 0 TOTALAA,
       0 TOTALSA,
       ISNULL(SUM(FNPLANCOMPROMISO),0)  OBJETIVO,
       0 TOTALREAL,
       FCNIVEL1PROD, 
       FCNIVEL1PROD,
       FINIVEL1PROD, 
       FINIVEL1PROD,
       p.NIVEL 
  FROM DETALLE_COLOCACION_PLAN c
 INNER JOIN EstructuraProductos p 
    ON c.finivel1prod=p.idProducto 
   AND p.Visible =1
 WHERE 1=1
   AND c.IDCC is not null
   AND c.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01' --semana actual
    --AND c.FidiaSem BETWEEN 1 AND 7
    --AND c.FIELEMENTO_PADRE = 10000007 --Condicion que se enlaza en caso de traer el parametro para formato
    --AND c.FISUPERIOR =dcp.FiIdOrigenCUC  --DEPENDE DEL TABULAR DE ORIGINACION
    --AND c.FICLASIFICACIONCTE IN(-1, 0, 1) --DEPENDE DEL FILTRO DE ....
 GROUP BY FCNIVEL1PROD, FINIVEL1PROD ,NIVEL
 UNION 
  ---------------------------------------------------------TOTAL REAL
SELECT 0 TOTALAA,
       0 TOTALSA,
       0 OBJETIVO,
       isnull(sum(FnColocaBruta),0) TOTALREAL,
       FCNIVEL1PROD,
       FCNIVEL2PROD, 
       FINIVEL1PROD,
       FINIVEL2PROD,
       p.NIVEL 
  FROM COLOCACION_AGRUPADA c 
 INNER JOIN EstructuraProductos p 
    ON c.finivel2prod=p.idProducto 
   AND p.Visible =1
 WHERE 1=1 
   AND c.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01' --semana actual
   AND c.ESTERRITORIO = 1
   --AND c.FidiaSem BETWEEN 1 AND 7
   --AND c.FIELEMENTO_PADRE = 10000007 --Condicion que se enlaza en caso de traer el parametro para formato
   --AND C.FISUPERIOR = C.FIORIGEN --DEPENDE DEL TABULAR DE ORIGINACION
   --AND C.FICLASIFICACIONCTE IN(-1, 0, 1) --DEPENDE DEL FILTRO DE ....
 GROUP BY FCNIVEL1PROD, FCNIVEL2PROD, FINIVEL1PROD, FINIVEL2PROD, NIVEL
 UNION
SELECT 0 TOTALAA,
       0 TOTALSA,
       0 OBJETIVO,
       isnull(sum(FnColocaBruta),0)TOTALREAL,
       FCNIVEL1PROD, 
       FCNIVEL1PROD,
       FINIVEL1PROD, 
       FINIVEL1PROD,
       p.NIVEL 
  FROM COLOCACION_AGRUPADA c 
 INNER JOIN EstructuraProductos p 
    ON c.finivel1prod=p.idProducto 
   AND p.Visible =1 
 WHERE 1=1
  AND c.FDFECHA BETWEEN '2021-07-26' AND '2021-08-01' --semana actual
  AND c.ESTERRITORIO = 1
  --AND c.FidiaSem BETWEEN 1 AND 7
  --AND c.FIELEMENTO_PADRE = 10000007 --Condicion que se enlaza en caso de traer el parametro para formato
  --AND C.FISUPERIOR = C.FIORIGEN --DEPENDE DEL TABULAR DE ORIGINACION
  --AND C.FICLASIFICACIONCTE IN(-1, 0, 1) --DEPENDE DEL FILTRO DE ....
 GROUP BY FCNIVEL1PROD, FINIVEL1PROD ,NIVEL
 ORDER BY FINIVEL1PROD,NIVEL desc
  ) AS AC
 GROUP BY FINIVEL1PROD,FINIVEL2PROD,FCNIVEL2PROD,NIVEL 
 ORDER by FINIVEL1PROD,NIVEL desc
 
 
 
 
 
create projection db_miscartera.PROJECT_GRAFICA_OBJETIVO
(
FnPlanCompromiso,
 FDFECHA,
 FiIdClasificacionCTE,
 FISUPERIOR,
 FiIdOrigenCUC  
) AS
SELECT
 FDFECHA,
 FiIdClasificacionCTE,
 FISUPERIOR,
 FiIdOrigenCUC,
 SUM(FnPlanCompromiso)  
FROM DETALLE_COLOCACION_PLAN
GROUP BY FDFECHA,FiIdClasificacionCTE,FISUPERIOR,FiIdOrigenCUC
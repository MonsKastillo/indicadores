--DROP VIEW vw_Dimgeovirtual_Esp

CREATE VIEW vw_Dimgeovirtual_Esp
AS
SELECT 
         A.FN_IDPAIS                                      
         ,A.FC_DESCPAIS                                          
         ,CAST(A.FI_IDCC AS INT) IDCC                    
         ,CAST(A.FI_IDCENTRO AS INT)  as IDCENTRO                                      
         ,CAST(A.FI_IDCC AS VARCHAR(6)) || ' - ' || A.FC_NOMBRE  AS NOMBRE                             
         ,A.FN_IDTIPOOP                                          
         ,A.FN_TIPOCC                                          
         ,A.FN_STATUS                                          
         ,CAST(A.FI_CENTROUP AS INT) CENTROSUP                  
         ,A.FI_IDCANAL                                          
         ,A.FC_STCENTRO                                          
         ,A.FI_BNCAPTACION                                          
         ,A.FI_BNCAPTACIONREG                                          
         ,A.FI_BNCAPTACIONZONA                                          
         ,A.FI_BNCAPTACIONPAIS                                          
         ,A.FI_BNCOLOCACION                                          
         ,A.FI_BNCOLOCACIONREG                                          
         ,A.FI_BNCOLOCACIONZONA                                          
         ,A.FI_BNCOLOCACIONPAIS                                          
         ,A.FI_IDESTADO                                          
         ,A.FI_DSESTADO                                          
         ,A.FI_IDFORMATO                                          
         ,A.FI_ESCANALTERCEROS                                          
         ,A.FN_CODENTIDAD                                          
         ,A.FI_ESALNOVA                                          
         ,A.FI_ESSUCALNOVA                                          
         ,A.FI_ESNIVEL                                          
         ,A.FC_DESC_FORMATO                                          
         ,A.FC_TIPODESPLIEGA                                          
         ,A.FD_FECHAAPERTURA                                          
         ,A.FD_FECHACIERRE                                          
         ,A.FI_TIENEFECHA                                          
         ,A.FC_IVA                                          
         ,A.FC_IDAPERTURA                                          
         ,A.FC_APERTURAS                                          
         ,A.FI_IDTICHY        
         ,A.FI_TICHY                                          
         ,CASE FI_IDTICHY  WHEN '3' THEN '0 - ' || FI_TICHY 
                           WHEN '1' THEN '127 - OPERACION GEOGRAFICA MEXICO' 
                           WHEN '2' THEN '2 - ' || FI_TICHY 
          END AS descrTichy                                          
         ,A.FI_IDAGENTE                                          
         ,A.FC_AGENTE                                          
         ,A.FN_TIPOSUCURSAL                                          
         ,A.FC_NOMTIPOSUCURSAL                                          
         ,A.FI_GASTO                                          
         ,A.FI_IDGASTO                                          
         ,A.FI_IDCCALNOVA                                          
         ,A.FI_ESTRUCTURA                                          
         ,A.FC_NOMBREESTRUCTURA                                          
         ,CASE WHEN A.FI_IDCC = '776487' THEN '22' ELSE A.FC_ESTRUCTURACVE            
          END ESTRUCTURACVE                                        
         ,A.FI_UNeg                                            
         ,CASE  WHEN A.FC_TIPODESPLIEGA = 'V'AND A.FN_IDTIPOOP = 4 /*PUNTO DE VENTA*/ THEN 1 
                ELSE 0 
          END AS Sucursal  
         ,1 mes                                                
         ,1 trimestreEKT                                                
         ,2020 anioSem                                                
         ,1 AS semanaekt                                                
         ,1 dia                                                
         ,20200101 idFecha                                                
         ,202001 AS aniomes                                                
         ,202001 AS aniosemsem                                                
         ,202001 AS aniotrim    
         ,0 AS TipoCambio    
         ,A.FN_IDFORMATOU                             
         ,0 AS idLocalidad                             
         ,A.FC_STATUS                        
         ,A.FC_FORMATOID                        
         ,A.FC_FORMATO                        
   FROM  DimGeoPresentacion  AS A                                       
  WHERE  A.FC_ESTRUCTURACVE = 2 
    AND  A.FN_STATUS IN ('1','5','6','4','7','9') 
    AND  A.FI_ESTRUCTURA = '8'  
    AND  ((A.FN_TIPOSUCURSAL =1 AND A.FC_ESTRUCTURACVE IN ('2')) OR (A.FN_TIPOSUCURSAL = 2 AND A.FC_ESTRUCTURACVE ='12')) 
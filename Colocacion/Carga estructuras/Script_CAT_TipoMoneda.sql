CREATE TABLE db_miscartera.CAT_TipoMoneda
(
    Fi_IdTipoMoneda int,
    Fc_DescTipoMoneda varchar(25),
    Fb_Status boolean
);


INSERT INTO db_miscartera.CAT_TipoMoneda (Fi_IdTipoMoneda,Fc_DescTipoMoneda,Fb_Status) VALUES (1,'Moneda Local',true);
INSERT INTO db_miscartera.CAT_TipoMoneda (Fi_IdTipoMoneda,Fc_DescTipoMoneda,Fb_Status) VALUES (2,'Pesos Mexicanos',true);
INSERT INTO db_miscartera.CAT_TipoMoneda (Fi_IdTipoMoneda,Fc_DescTipoMoneda,Fb_Status) VALUES (3,'Dolares',true);
CREATE TABLE db_miscartera.paso_geovirtual_detalle
(
    IDCC int,
    IDCENTRO int,
    IDFORMATOU int,
    FIELEMENTO_PADRE int,
    EsTerritorio int
);

--correr cuando se actualize la geografia
INSERT INTO paso_geovirtual_detalle
SELECT IDCC,IDCENTRO,NULL,NULL FROM vw_dimgeovirtual_Esp

	update paso_geovirtual_detalle
   SET IDFORMATOU = A.FN_IDFORMATOU
  FROM VW_DIMGEOVIRTUAL_ESP A
 WHERE paso_geovirtual_detalle.IDCC= A.IDCC
 
  update paso_geovirtual_detalle
   SET FIELEMENTO_PADRE = A.FIELEMENTO_PADRE
  FROM dimcolformatou A
 WHERE paso_geovirtual_detalle.IDFORMATOU= A.FIELEMENTO
 
 UPDATE paso_geovirtual_detalle
       SET ESTERRITORIO = 1
      FROM vw_Dimgeovirtual_Esp vde 
INNER JOIN NIVELESGEOGRAFIA_AIE na 
		ON vde.IDCC = na.CENTROCOSTOS 
	   AND NA.NIVEL_6 LIKE '%TERRITORIO%'
	 WHERE paso_geovirtual_detalle.IDCC = vde.IDCC
	 
	 
   UPDATE paso_geovirtual_detalle
       SET ESTERRITORIO = 0
	 WHERE EsTerritorio ISNULL 

  UPDATE DETALLE_COLOCACION_PLAN
    SET FIELEMENTO = A.IDFORMATOU
   FROM paso_geovirtual_detalle A
  WHERE DETALLE_COLOCACION_PLAN.IDCC = A.IDCC
 
UPDATE DETALLE_COLOCACION_PLAN
    SET FIELEMENTO_PADRE = A.FIELEMENTO_PADRE
   FROM paso_geovirtual_detalle A
  WHERE DETALLE_COLOCACION_PLAN.FIELEMENTO= A.IDFORMATOU

    
/*  
  select * from NIVELESGEOGRAFIA_AIE na  where CENTROCOSTOS  = 554051
  select * from vw_Dimgeovirtual_Esp ve  where idcc  = 554051
  
   select * from NIVELESGEOGRAFIA_AIE na  where CENTROCOSTOS  = 582385
  select * from vw_Dimgeovirtual_Esp ve  where idcc  = 582385

  select FC_ESTRUCTURACVE, fn_status, fi_estructura, FN_TIPOSUCURSAL from DimGeoPresentacion dgp where FI_IDCC in('554051', '582385')
	  
  truncate table NIVELESGEOGRAFIA_AIE 
	*/ 
	 
	 
 ALTER TABLE DETALLE_COLOCACION_LIQD ADD COLUMN ESTERRITORIO  INT
 DEFAULT  ( SELECT A.esterritorio FROM db_miscartera.paso_geovirtual_detalle A WHERE  A.IDCENTRO = DETALLE_COLOCACION_LIQD.FISUCURSAL)


COPY db_miscartera.paso_geovirtual_detalle FROM '~/paso_geovirtual_detalle.csv' DELIMITER ',';


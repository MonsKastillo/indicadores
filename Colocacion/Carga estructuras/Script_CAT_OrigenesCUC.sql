-- db_miscartera.CAT_OrigenesCUC definition

CREATE TABLE db_miscartera.CAT_OrigenesCUC
(
    Fi_IDOrigen int,
    FC_DescOrigen varchar(50),
    Fi_IDNivelSuperior int,
    Fb_Status boolean,
    Fi_Superior int,
    EsSucursal int,
    FCSUCURSAL varchar(50)
);

INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (4,'SEGUROS',999,true,4,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (7,'FACTURACION PV BAZ',999,true,7,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (8,'FACTURACION CASH BACK',999,true,8,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (9,'FACTURACION INTERNA',999,true,9,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (11,'FACTURACION RETIROS EN VENTANILLA',999,true,11,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (13,'CONTANET',999,true,13,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (15,'MEDIOS DE PAGO',999,true,15,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (16,'PLANEACION',999,true,16,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (24,'GESTION REMOTA',200,true,24,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (200,'BANCA DIGITAL',100,true,200,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES  (999,'TODOS LOS ORIGENES',0,true,999,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (22,'ITALIKA.MX',100,true,22,22,'ITALIKA.MX');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (1,'SIE',999,true,1,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (6,'FACTURACION EXTERNA',999,true,6,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (14,'AS/400',999,true,14,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (20,'REPROGRAMACIONES MAS',999,true,20,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (23,'ASESORADO',200,true,23,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (25,'ONLINE/OFFLINE',200,true,25,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (26,'100% ONLINE',200,true,26,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (27,'100% ONLINE ORGANICO',200,true,27,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES  (12,'BANCA DIGITAL',200,true,12,12,'BANCA DIGITAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (17,'ELEKTRA.COM',100,true,17,17,'ELEKTRA.COM');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (30,'SUPER APP',999,true,30,30,'SUPER APP');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (-1,'No Asignado',0,false,-1,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (2,'REGIONAL',999,true,2,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (3,'ALNOVA',999,true,3,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (5,'MILENIA',999,true,5,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (10,'FACTURACION RETIROS EN CAJEROS',999,true,10,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (18,'ATM',999,true,18,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (19,'REPROGRAMACIONES',999,true,19,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (29,'MERCADO ABIERTO',11,true,29,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (100,'MEDIOS DIGITALES',999,true,100,300,'SUCURSAL');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (21,'ONE CLICK',100,true,12,21,'ONE CLICK');
INSERT INTO db_miscartera.CAT_OrigenesCUC (Fi_IDOrigen,FC_DescOrigen,Fi_IDNivelSuperior,Fb_Status,Fi_Superior,EsSucursal,FCSUCURSAL) VALUES	 (28,'EMPLEADO',11,true,28,28,'EMPLEADO');
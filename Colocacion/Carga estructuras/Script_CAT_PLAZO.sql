CREATE TABLE db_miscartera.CAT_PLAZO
(
    Fi_Plazo int,
    Fi_Rango int,
    Fc_Plazo varchar(15)
);

COPY CAT_PLAZO FROM '/vertica/data/VMart/CAT_PLAZO.csv' DELIMITER ',';
CREATE TABLE db_miscartera.DETALLE_COLOCACION_PLAN
(
    IDCC int,
    FiSucursal int,
    FiIdFecha int,
    FiIdProducto varchar(30),
    FiIdTipoInfo int,
    FiIdFormato varchar(20),
    FiIdPais int,
    FiIdPlazo int,
    FiIdOrigen varchar(2),
    FiIdOrigenCUC int,
    FcSKU varchar(20),
    FiPlanColocacion int,
    FiIdapertura varchar(20),
    IDFORMATOU numeric(16,0),
    FnPlanCompromiso float,
    FiIdFormatoID varchar(20),
    FiIdTipoMoneda int,
    FICLASIFICACIONCTE int,
    FnMNPLANENGANCHE float,
    FnMNPLANLIQRENOVACION float,
    FnMNPLANLIQANTICIPADA float,
    FDFECHA date DEFAULT ( SELECT DimPeriodos.fecha AS FECHA
 FROM db_miscartera.DimPeriodos
 WHERE (DimPeriodos.idFecha = DETALLE_COLOCACION_PLAN.FiIdFecha)) SET USING ( SELECT DimPeriodos.fecha AS FECHA
 FROM db_miscartera.DimPeriodos
 WHERE (DimPeriodos.idFecha = DETALLE_COLOCACION_PLAN.FiIdFecha)),
    FCDescOrigen varchar(100) DEFAULT ( SELECT a.FC_DescOrigen
 FROM db_miscartera.CAT_OrigenesCUC a
 WHERE (a.Fi_IDOrigen = DETALLE_COLOCACION_PLAN.FiIdOrigenCUC)),
    FISUPERIOR int DEFAULT ( SELECT CAT_OrigenesCUC.Fi_Superior
 FROM db_miscartera.CAT_OrigenesCUC
 WHERE (CAT_OrigenesCUC.Fi_IDOrigen = DETALLE_COLOCACION_PLAN.FiIdOrigenCUC)) SET USING ( SELECT CAT_OrigenesCUC.Fi_Superior
 FROM db_miscartera.CAT_OrigenesCUC
 WHERE (CAT_OrigenesCUC.Fi_IDOrigen = DETALLE_COLOCACION_PLAN.FiIdOrigenCUC)),
    EsSucursal int DEFAULT ( SELECT CAT_OrigenesCUC.EsSucursal
 FROM db_miscartera.CAT_OrigenesCUC
 WHERE (CAT_OrigenesCUC.Fi_IDOrigen = DETALLE_COLOCACION_PLAN.FiIdOrigenCUC)) SET USING ( SELECT CAT_OrigenesCUC.EsSucursal
 FROM db_miscartera.CAT_OrigenesCUC
 WHERE (CAT_OrigenesCUC.Fi_IDOrigen = DETALLE_COLOCACION_PLAN.FiIdOrigenCUC)),
    FCSUCURSALORIGEN varchar(50) DEFAULT ( SELECT CAT_OrigenesCUC.FCSUCURSAL
 FROM db_miscartera.CAT_OrigenesCUC
 WHERE (CAT_OrigenesCUC.Fi_IDOrigen = DETALLE_COLOCACION_PLAN.FiIdOrigenCUC)) SET USING ( SELECT CAT_OrigenesCUC.FCSUCURSAL
 FROM db_miscartera.CAT_OrigenesCUC
 WHERE (CAT_OrigenesCUC.Fi_IDOrigen = DETALLE_COLOCACION_PLAN.FiIdOrigenCUC)),
    FiDiaSem int DEFAULT ( SELECT DimPeriodos.diaSem
 FROM db_miscartera.DimPeriodos
 WHERE (DimPeriodos.idFecha = DETALLE_COLOCACION_PLAN.FiIdFecha)),
    FcDescDiaSem varchar(50) DEFAULT ( SELECT replace((A.DescDiaSem), '"', '') AS REPLACE
 FROM db_miscartera.DimPeriodos A
 WHERE (A.idFecha = DETALLE_COLOCACION_PLAN.FiIdFecha)),
    FiSemana int DEFAULT ( SELECT DimPeriodos.semana
 FROM db_miscartera.DimPeriodos
 WHERE (DimPeriodos.idFecha = DETALLE_COLOCACION_PLAN.FiIdFecha)),
    FiAnioSem int DEFAULT ( SELECT DimPeriodos.anioSem
 FROM db_miscartera.DimPeriodos
 WHERE (DimPeriodos.idFecha = DETALLE_COLOCACION_PLAN.FiIdFecha)),
    FIELEMENTO int,
    FCELEMENTO varchar(100) DEFAULT ( SELECT DIMCOLFORMATOU.FCELEMENTO
 FROM db_miscartera.DIMCOLFORMATOU
 WHERE (DIMCOLFORMATOU.FIELEMENTO = DETALLE_COLOCACION_PLAN.FIELEMENTO)) SET USING ( SELECT DIMCOLFORMATOU.FCELEMENTO
 FROM db_miscartera.DIMCOLFORMATOU
 WHERE (DIMCOLFORMATOU.FIELEMENTO = DETALLE_COLOCACION_PLAN.FIELEMENTO)),
    FIELEMENTO_PADRE int,
    FCELEMENTO_PADRE varchar(100) DEFAULT ( SELECT DIMCOLFORMATOU.FCELEMENTO_PADRE
 FROM db_miscartera.DIMCOLFORMATOU
 WHERE ((DIMCOLFORMATOU.FIELEMENTO_PADRE = DETALLE_COLOCACION_PLAN.FIELEMENTO_PADRE) AND (DIMCOLFORMATOU.FIELEMENTO = DETALLE_COLOCACION_PLAN.FIELEMENTO))) SET USING ( SELECT DIMCOLFORMATOU.FCELEMENTO_PADRE
 FROM db_miscartera.DIMCOLFORMATOU
 WHERE ((DIMCOLFORMATOU.FIELEMENTO_PADRE = DETALLE_COLOCACION_PLAN.FIELEMENTO_PADRE) AND (DIMCOLFORMATOU.FIELEMENTO = DETALLE_COLOCACION_PLAN.FIELEMENTO))),
    FINIVEL1PROD varchar(20) DEFAULT ( SELECT a.ID_N1
 FROM db_miscartera.NIVELESPRODUCTO a
 WHERE (DETALLE_COLOCACION_PLAN.FiIdProducto = (a.ID_PRODUCTO)::!varchar)),
    FCNIVEL1PROD varchar(60) DEFAULT ( SELECT a.NIVEL_1
 FROM db_miscartera.NIVELESPRODUCTO a
 WHERE (DETALLE_COLOCACION_PLAN.FiIdProducto = (a.ID_PRODUCTO)::!varchar)),
    FINIVEL2PROD varchar(20) DEFAULT ( SELECT a.ID_N2
 FROM db_miscartera.NIVELESPRODUCTO a
 WHERE (DETALLE_COLOCACION_PLAN.FiIdProducto = (a.ID_PRODUCTO)::!varchar)),
    FCNIVEL2PROD varchar(60) DEFAULT ( SELECT a.NIVEL_2
 FROM db_miscartera.NIVELESPRODUCTO a
 WHERE (DETALLE_COLOCACION_PLAN.FiIdProducto = (a.ID_PRODUCTO)::!varchar))
);

-- select * from DETALLE_COLOCACION_PLAN
--truncate table DETALLE_COLOCACION_PLAN
INSERT INTO db_miscartera.DETALLE_COLOCACION_PLAN
 SELECT ID_UNIT ::!INT
 ,G.IDCENTRO 
 ,Id_DIAAIE::!INT
  ,CASE    
  -------------TAZ-----------------------------------  
   WHEN ID_PRODUCTOMIS = '411000000' THEN '4400000'  
   WHEN ID_PRODUCTOMIS = '3000102' THEN '3000120'    
   WHEN ID_PRODUCTOMIS = '30000000' THEN '3000023'  
   ELSE ID_PRODUCTOMIS    
  END AS idProducto                             
 --,ID_PRODUCTOMIS AS idProducto                              
 ,2                              
 ,G.FI_IDFORMATO::!varchar                             
 ,Id_Pais AS IdPais                              
 ,3 AS IdPlazo                              
 ,'SU' AS IdOrigen                                   
 ,ID_ORIGEN as idOrigenCUC          
 ,ID_SKU SKU                                                         
 ,MNLOCAL_DIA as PlanColocacion        
 ,g.FC_IDAPERTURA
 ,FN_IDFORMATOU
 ,MNLOCAL_DIA                                                            
 ,G.FI_IDFORMATO                              
 ,2 AS IDTIPOMONEDA
 , -1                                
 , Enganche AS MNPLANENGANCHE  
 , LiquidacionRenovacion AS MNPLANLIQRENOVACION  
 ,LiquidacionAnticipada AS MNPLANLIQANTICIPADA  
 from db_miscartera.PLAN2021MIS_COLOCACION_MX A                   
INNER JOIN db_miscartera.vw_dimgeovirtual_Esp G                              
 ON A.ID_UNIT = G.IDCC
 
 
 --SELECT * FROM PLAN2021MIS_COLOCACION_MX
 




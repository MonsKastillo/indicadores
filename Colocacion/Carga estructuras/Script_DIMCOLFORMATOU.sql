-- db_miscartera.CAT_FORMATOU definition

CREATE TABLE db_miscartera.CAT_FORMATOU
(
    FDFECHAPROCESO date,
    FIANIO int,
    FISEMANA int,
    FIELEMENTO int,
    FCELEMENTO varchar(100),
    FIELEMENTO_PADRE int,
    FCELEMENTO_PADRE varchar(100),
    FCUSUARIO_CREACION varchar(50),
    FDFECHA_CREACION date,
    FCUSUARIO_MODIFICACION varchar(50),
    FDFECHA_MODIFICACION date
);

COPY CAT_FORMATOU FROM '/vertica/data/VMart/CAT_FORMATOU.csv' DELIMITER ',';
commit;
--Comandos para refrescar las proyecciones despues crearlas o borrarlas 
SELECT GET_PROJECTIONS('DETALLE_COLOCACION_PLAN');
SELECT START_REFRESH();

-- comando para refrescar las columnas de una tabla flat cuando se realiza una actualiza
 --se debe colocar el nombre de la tabla y en las siguientes commillas el nombre de la columna y si se deja vacio refresca toda la tabla.
SELECT REFRESH_COLUMNS ('DETALLE_COLOCACION_PLAN', '')

--Ejempl agregar un campo defaul a una tabla flat

ALTER TABLE  db_miscartera.DETALLE_COLOCACION add COLUMN IDCC INT       
DEFAULT (select A.IDCC FROM paso_geovirtual_detalle A WHERE ISNULL(DETALLE_COLOCACION.FISUCURSALVEND,ISNULL(DETALLE_COLOCACION.FISUCURSALEMP,DETALLE_COLOCACION.FISUCURSAL))=A.IDCENTRO)


--REFRESH DE CAMPOS

--TABLE DETALLE_COLOCACION
SELECT REFRESH_COLUMNS ('DETALLE_COLOCACION', 'FCELEMENTO_PADRE', 'UPDATE')
SELECT REFRESH_COLUMNS ('DETALLE_COLOCACION', 'FCELEMENTO', 'UPDATE')

----TABLE DETALLE_COLOCACION_PLAN
SELECT REFRESH_COLUMNS ('DETALLE_COLOCACION', 'FCELEMENTO_PADRE', 'UPDATE')
SELECT REFRESH_COLUMNS ('DETALLE_COLOCACION', 'FCELEMENTO', 'UPDATE')
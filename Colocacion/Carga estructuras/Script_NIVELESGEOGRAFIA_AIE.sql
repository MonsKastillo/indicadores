CREATE TABLE 
IF NOT EXISTS db_miscartera.NIVELESGEOGRAFIA_AIE
(
  CENTROCOSTOS INT,
  IDCENTRO INT,
  ESTRUCTURA INT,
  ID_N1 INT,
  ID_CC1 INT,
  NIVEL_1 VARCHAR(250),
  ID_N2 INT,
  ID_CC2 INT,
  NIVEL_2 VARCHAR(250),
  ID_N3 INT,
  ID_CC3 INT,
  NIVEL_3 VARCHAR(250),
  ID_N4 INT,
  ID_CC4 INT,
  NIVEL_4 VARCHAR(250),
  ID_N5 INT,
  ID_CC5 INT,
  NIVEL_5 VARCHAR(250),
  ID_N6 INT,
  ID_CC6 INT,
  NIVEL_6 VARCHAR(250),
  ID_N7 INT,
  ID_CC7 INT,
  NIVEL_7 VARCHAR(250),
  ID_N8 INT,
  ID_CC8 INT,
  NIVEL_8 VARCHAR(250),
  ID_N9 INT,
  ID_CC9 INT,
  NIVEL_9 VARCHAR(250),
  ID_N10 INT,
  ID_CC10 INT,
  NIVEL_10 VARCHAR(250),
  ID_N11 INT,
  ID_CC11 INT,
  NIVEL_11 VARCHAR(250),
  ID_N12 INT,
  ID_CC12 INT,
  NIVEL_12 VARCHAR(250),
  ID_N13 INT,
  ID_CC13 INT,
  NIVEL_13 VARCHAR(250)
  ID_N14 INT,
  ID_CC14 INT,
  NIVEL_14 VARCHAR(250)
  ID_N15 INT,
  ID_CC15 INT,
  NIVEL_15 VARCHAR(250)
)
CREATE TABLE db_miscartera.NIVELESPRODUCTO
(
    ID_PRODUCTO int,
    ID_N1 int,
    NIVEL_1 varchar(100),
    ID_N2 varchar(80),
    NIVEL_2 varchar(100),
    ID_N3 varchar(80),
    NIVEL_3 varchar(100),
    ID_N4 varchar(80),
    NIVEL_4 varchar(100),
    ID_N5 varchar(80),
    NIVEL_5 varchar(100)
);

COPY NIVELESPRODUCTO FROM '/vertica/data/VMart/NIVELESPRODUCTO.csv' DELIMITER ',';
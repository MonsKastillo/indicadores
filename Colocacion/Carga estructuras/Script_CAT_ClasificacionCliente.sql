-- db_miscartera.CAT_ClasificacionCliente definition

CREATE TABLE db_miscartera.CAT_ClasificacionCliente
(
    Fi_ClasificacionCTE int,
    Fc_ClasificacionCTE varchar(26)
);

INSERT INTO db_miscartera.CAT_ClasificacionCliente (Fi_ClasificacionCTE,Fc_ClasificacionCTE) VALUES (1,'CLIENTES RECOMPRA');
INSERT INTO db_miscartera.CAT_ClasificacionCliente (Fi_ClasificacionCTE,Fc_ClasificacionCTE) VALUES	 (0,'CLIENTES NUEVOS');
INSERT INTO db_miscartera.CAT_ClasificacionCliente (Fi_ClasificacionCTE,Fc_ClasificacionCTE) VALUES	 (-1,'CLIENTES SIN CLASIFICACION');
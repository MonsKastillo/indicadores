 CREATE TABLE db_miscartera.CAT_Reprogramaciones
(
    Fi_IdReprogramacion int,
    Fc_DescReprogramacion varchar(15)
);


INSERT INTO db_miscartera.CAT_Reprogramaciones (Fi_IdReprogramacion,Fc_DescReprogramacion) VALUES	 (1,'Aguantame');
INSERT INTO db_miscartera.CAT_Reprogramaciones (Fi_IdReprogramacion,Fc_DescReprogramacion) VALUES	 (3,'Pagos Ligeros');
INSERT INTO db_miscartera.CAT_Reprogramaciones (Fi_IdReprogramacion,Fc_DescReprogramacion) VALUES	 (4,'Restitucion');
INSERT INTO db_miscartera.CAT_Reprogramaciones (Fi_IdReprogramacion,Fc_DescReprogramacion) VALUES	 (7,'VTI No BAZ');
INSERT INTO db_miscartera.CAT_Reprogramaciones (Fi_IdReprogramacion,Fc_DescReprogramacion) VALUES	 (0,'No Aplica');
INSERT INTO db_miscartera.CAT_Reprogramaciones (Fi_IdReprogramacion,Fc_DescReprogramacion) VALUES	 (2,'Aguantame +');
INSERT INTO db_miscartera.CAT_Reprogramaciones (Fi_IdReprogramacion,Fc_DescReprogramacion) VALUES	 (5,'Congelame');
INSERT INTO db_miscartera.CAT_Reprogramaciones (Fi_IdReprogramacion,Fc_DescReprogramacion) VALUES	 (6,'VTI BAZ');
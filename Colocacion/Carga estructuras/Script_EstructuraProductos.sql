CREATE TABLE db_miscartera.EstructuraProductos
(
    idProducto varchar(20),
    cgProducto varchar(250),
    idProductoSup varchar(50),
    nivel int,
    Visible int
);

COPY EstructuraProductos FROM '/vertica/data/VMart/EstructuraProductos.csv' DELIMITER ',';
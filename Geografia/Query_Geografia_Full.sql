DROP TABLE #GEOGRAFIA
DROP TABLE #GEOGRAFIAFINAL2
DROP TABLE #GEOGRAFIAFINAL3
DROP TABLE #tmpR
DROP TABLE #pASoGeoPruebaFinal
DROP TABLE #MAXNiveles
DROP TABLE #NivelMAX
DROP TABLE #tmpCC
DROP TABLE #tmpCentros


DECLARE	@CENTROSUP AS INT,
	    @CENTRO_PAD AS INT ,
	    @ID_CENTRO AS INT,
	    @SIGUIENTE AS INT,
	    @CONT AS INT, 
	    @CONT2 AS INT,
	    @tieneID INT

 SELECT
	    1 NIVEL,
        ROW_NUMBER() OVER (ORDER BY IDCC) ID,
        IDCC,
        IDCENTRO,
        NOMBRE,
        CENTROSUP,
        AGENTE,
        NOMBREESTRUCTURA,
        ESTRUCTURA
   INTO #GEOGRAFIA
   FROM Vw_Dimgeovirtual_Esp A
  WHERE 
        ESTRUCTURACVE = 2 
    AND STATUS IN ('1','5','6','4','7','9') 
	AND ESTRUCTURA = '8'  
    AND ((TIPOSUCURSAL =1 AND ESTRUCTURACVE IN ('2')) OR (TIPOSUCURSAL =2 AND ESTRUCTURACVE ='12')) 
    AND IDCENTRO IN(
	4051, 2385
	)

	SET @CONT2 = (SELECT COUNT(1) FROM #GEOGRAFIA)

	SET @CONT = 1


WHILE @CONT2 >= @CONT
BEGIN

    SELECT @CENTROSUP = CENTROSUP, 
	       @ID_CENTRO = IDCENTRO,  --@SIGUIENTE = IDCENTRO, 
	       @tieneID = IDCC 
      FROM #GEOGRAFIA 
	 WHERE ID = @CONT

    SELECT @CENTRO_PAD = IDCC 
	  FROM Vw_Dimgeovirtual_Esp 
	 WHERE IDCC = 102

    WHILE @CENTROSUP <> @CENTRO_PAD
    BEGIN
        

          INSERT INTO #GEOGRAFIA
          SELECT 0, @tieneID, IDCC, IDCENTRO, NOMBRE, CENTROSUP, AGENTE, NOMBREESTRUCTURA, ESTRUCTURA
            FROM Vw_Dimgeovirtual_Esp 
		   WHERE IDCC =@CENTROSUP 

          SELECT @CENTROSUP = CENTROSUP, 
				 @ID_CENTRO = IDCENTRO 
		    FROM Vw_Dimgeovirtual_Esp 
		   WHERE IDCC =@CENTROSUP
    END
    
    INSERT INTO #GEOGRAFIA
    SELECT 0, @tieneID, IDCC, IDCENTRO, NOMBRE, CENTROSUP, AGENTE, NOMBREESTRUCTURA, ESTRUCTURA
      FROM Vw_Dimgeovirtual_Esp 
	 WHERE IDCC = 102

    SET @CONT += 1
  
END

SELECT NIVEL, CASE WHEN NIVEL = 1 THEN IDCC ELSE ID END SUCURSAL, IDCC, IDCENTRO, NOMBRE, CENTROSUP,  ESTRUCTURA
INTO #GEOGRAFIAFINAL2
FROM #GEOGRAFIA 

SELECT ROW_NUMBER() OVER (PARTITION BY SUCURSAL ORDER BY NIVEL DESC) NIVEL, 
	   SUCURSAL, 
	   IDCC, 
	   IDCENTRO, 
	   NOMBRE, 
	   CENTROSUP, 
	   ESTRUCTURA
	   --NIVEL
  INTO #GEOGRAFIAFINAL3
  FROM #GEOGRAFIAFINAL2
  --WHERE --IDCENTRO = 215
  --SUCURSAL = 550215 -- 550100


--------Sentencias de Jorge adaptadas a este query ------

  SELECT sucursal IDENTIFICADOR, 
	     COUNT(NIVEL) IDMODIFICADOR
	INTO #tmpR
	FROM #GEOGRAFIAFINAL3
	--WHERE SUCURSAL = 550215
GROUP BY sucursal


    UPDATE #GEOGRAFIAFINAL3 
	   SET NIVEL =  (R.IDMODIFICADOR - F.NIVEL ) + 1
      FROM  #GEOGRAFIAFINAL3 F 
INNER JOIN #tmpR R 
        ON R.IDENTIFICADOR = F.SUCURSAL

    SELECT  
			SUCURSAL AS idCeco,
			IDCENTRO AS idCentro,
			IDCC,
			NOMBRE AS descCeco,
			CENTROSUP AS idCecoSuperior,
			NIVEL AS idNivel
    INTO #pasoGeoPruebaFinal
    FROM #GEOGRAFIAFINAL3
	--WHERE SUCURSAL = 550215

	SELECT *
	  INTO #MAXNiveles 
      FROM #pasoGeoPruebaFinal  
  ORDER BY  idCeco, idNivel

  SELECT idCeco,
         MAX(idNivel) AS idNivelMAX
    INTO #NivelMAX 
    FROM #pasoGeoPruebaFinal 
	--where idCeco = 550215
GROUP BY idCeco


DECLARE @NivelMAX INT, @NivelMIN INT
DECLARE @variable VARCHAR(MAX)
DECLARE @variable2 VARCHAR(MAX)
DECLARE @sql VARCHAR(MAX)
DECLARE @VaridCentro VARCHAR(MAX)
DECLARE @tablatmpRecNivel   VARCHAR(MAX)
DECLARE @tablatmCentros    VARCHAR(MAX)
DECLARE @tablatmCC    VARCHAR(MAX)
DECLARE @idcc VARCHAR(MAX)


    SET @variable=''
    SET @variable2=''
    SET @VaridCentro=''
    SET @tablatmpRecNivel=''
    SET @tablatmCentros=''
	SET @tablatmCC=''
	SET @idcc = ''
    SET @sql=''

--establezco el nivel MINimo y MAXimo
SELECT @NivelMAX = MAX(idNivel), 
       @NivelMIN = MIN(idNivel) 
  FROM #MAXNiveles


WHILE(@NivelMIN<=@NivelMAX)
BEGIN

	IF @NivelMIN=@NivelMAX
	BEGIN
		SET @variable=@variable+'['+CAST(@NivelMIN AS VARCHAR(20))+']'
		SET @variable2=@variable2+ '['+CAST(@NivelMIN AS VARCHAR(20))+']'+ ' AS Nivel_' +CAST(@NivelMIN AS VARCHAR(20))
		SET @VaridCentro=@VaridCentro+ '['+CAST(@NivelMIN AS VARCHAR(20))+']'+ ' AS idCostoN_' +CAST(@NivelMIN AS VARCHAR(20))
		SET @idcc = @idcc + '['+CAST(@NivelMIN AS VARCHAR(20))+']'+ ' AS idCCN_' +CAST(@NivelMIN AS VARCHAR(20))

		SET @tablatmpRecNivel=@tablatmpRecNivel+ 'Nivel_' +CAST(@NivelMIN AS VARCHAR(20)) + ' VARCHAR(200)'
		SET @tablatmCentros=@tablatmCentros+ 'idCostoN_' +CAST(@NivelMIN AS VARCHAR(20)) + ' int'
		SET @tablatmCC = @tablatmCC + 'idCCN_' +CAST(@NivelMIN AS VARCHAR(20)) + ' int'
		
		
	END 
	ELSE
	BEGIN
		SET @variable=@variable+'['+CAST(@NivelMIN AS VARCHAR(20))+'],'
		SET @variable2=@variable2+'['+CAST(@NivelMIN AS VARCHAR(20))+']'+ ' AS Nivel_' +CAST(@NivelMIN AS VARCHAR(20))+ ','
		SET @VaridCentro=@VaridCentro+ '['+CAST(@NivelMIN AS VARCHAR(20))+']'+ ' AS idCostoN_' +CAST(@NivelMIN AS VARCHAR(20))+ ','
		SET @idcc = @idcc + '['+CAST(@NivelMIN AS VARCHAR(20))+']'+ ' AS idCCN_' +CAST(@NivelMIN AS VARCHAR(20)) + ','

		SET @tablatmpRecNivel=@tablatmpRecNivel+ 'Nivel_' +CAST(@NivelMIN AS VARCHAR(20))+ ' VARCHAR(200) ,'
		SET @tablatmCentros=@tablatmCentros+ 'idCostoN_' +CAST(@NivelMIN AS VARCHAR(20))+ ' int ,'
		SET @tablatmCC = @tablatmCC + 'idCCN_' +CAST(@NivelMIN AS VARCHAR(20)) + ' int ,'
		
	END

	SET @NivelMIN=@NivelMIN+1
END

--ejecuto la sentencia con PIVOT
--Genera los niveles por ceco
SET @sql='CREATE table #tmpRecNivel ( idCeco int , ' + @tablatmpRecNivel + ' )
INSERT into #tmpRecNivel
SELECT idCeco,'+@variable2+' '+'

FROM
(
	SELECT idCeco, idNivel,descCeco
	FROM #MAXNiveles
) AS p
PIVOT
(	
	MIN(descCeco)
	FOR idNivel in ('+@variable+')
) AS Pvt'
--SELECT  @sql
--exec(@sql)

--SET @sql = ''
--Genera los centrospor cada nivel
SET @sql=@sql + ' 
CREATE table #tmpCentros ( idCeco int , ' + @tablatmCentros + ')
INSERT into #tmpCentros
SELECT idCeco,'+@VaridCentro+' '+'
FROM
(
	SELECT idCeco, idNivel,idCentro
	FROM #MAXNiveles
) AS p
PIVOT
(	
	MIN(idCentro)
	FOR idNivel IN ('+@variable+')
) AS Pvt 
'
--select @sql
--exec (@sql)

--Genera los centros de costo por cada nivel
SET @sql=@sql + ' 
CREATE TABLE #tmpCC ( idCeco INT , ' + @tablatmCC + ')
INSERT INTO #tmpCC
SELECT idCeco,'+@idcc+' '+'
FROM
(
	SELECT idCeco, idNivel,idcc
	FROM #MAXNiveles
) AS p
PIVOT
(	
	MIN(idcc)
	FOR idNivel IN ('+@variable+')
) AS Pvt 
'
--select @sql
--exec (@sql)

DECLARE @CostoNivelMAX int, @CostoNivelMIN int
DECLARE @variableCon	VARCHAR(MAX)
DECLARE @variableSel	VARCHAR(MAX)
DECLARE @variableCC	VARCHAR(MAX)
DECLARE @sSql			VARCHAR(MAX)

SELECT @CostoNivelMAX=MAX(idNivelMAX) FROM #NivelMAX
SET @CostoNivelMIN = 1
SET @variableCon = ''
SET @sSql = ''
SET @variableSel = ''
SET @variableCC = ''

WHILE(@CostoNivelMIN<=@CostoNivelMAX)
BEGIN

	IF @CostoNivelMIN=@CostoNivelMAX
	BEGIN
		SET @variableCon=@variableCon+ ' FiIdCentroN_' +CAST(@CostoNivelMIN AS VARCHAR(20)) + ' int,' + ' FcDescCeco_' +CAST(@CostoNivelMIN AS VARCHAR(20)) + ' VARCHAR(200)'
		SET @variableSel=@variableSel+ ' idCostoN_' +CAST(@CostoNivelMIN AS VARCHAR(20)) + ',' + ' idCCN_' +CAST(@CostoNivelMIN AS VARCHAR(20)) + ',' + ' Nivel_' +CAST(@CostoNivelMIN AS VARCHAR(20))
		
	END 
	ELSE
	BEGIN
		SET @variableCon=@variableCon+ ' FiIdCentroN_' +CAST(@CostoNivelMIN AS VARCHAR(20)) + ' int, ' + ' FcDescCeco_' +CAST(@CostoNivelMIN AS VARCHAR(20))+ ' VARCHAR(200),'
		SET @variableSel=@variableSel+ ' idCostoN_' +CAST(@CostoNivelMIN AS VARCHAR(20)) + ',' + ' idCCN_' +CAST(@CostoNivelMIN AS VARCHAR(20)) + ',' + ' Nivel_' +CAST(@CostoNivelMIN AS VARCHAR(20)) + ', '

	END

	SET @CostoNivelMIN=@CostoNivelMIN+1
END

SET @sql=  @sql + ' 
--CREATE table #PasoFinal( FiIdCeco int, ' + @variableCon  + ' )	  
--INSERT into #PasoFinal	
SELECT B.idCeco, ' + @variableSel + ' 
--into #AUX_G3
FROM #tmpRecNivel A
INNER JOIN #tmpCentros B on B.idCeco = A.idCeco 
INNER JOIN #tmpCC C on C.idCeco = A.idCeco 

'

exec(@sql)



